## 4-20mA CURRENT LOOP
**Of all the analog signal, that can be used to transmit process information, the 4-20 mA is the dominant standard in the industry**   

![](https://gitlab.com/Utsav27/iotmodule2/-/raw/master/assignments/summary/Images/components.png) 
**COMPONENTS OF A 4-20mA CURRENT LOOP**  
_1) Sensor_  
_2) Transmitter_  
_3) Power Source_     
_4) Loop_  
_5) Receiver_

**PRONS AND CONS OF 4-20 mA CURRENT LOOP**  

|Prons|Cons|
|-----|----|
|The 4-20 mA current Loop is dominant standard in many industries|Current loop can transmit one particular process signal|
|It is the simplest option to connect and configure|Multiple loops must be created in situations where there are numerous process variables that require transmission. Running so much wire could lead to problems with ground loops if independent loops are not properly isolated.|
|It uses less wiring and connections than other signals, greatly reducing initial setup costs.|These isolation requirements become exponentially more complicated as the number of loops increases.|
|It is less sensitive to background electrical noise.||
|Since 4 mA is equal to 0% output, it is incredibly simple to detect a fault in the system.||

# MODBUS COMMUNICATION PROTOCOL   
* Modbus is a communication protocol used with Programmable Logic Controllers(PLC's).
* Modbus provides a common language for devices to communicate with one another.
* Modbus device works using Master-Slave technique, which has one master and upto 247 slaves.
* Master can individually address slave or initiate a brodcast messages to all slaves, slave return responds to all message queries address to them individually, but do not respond to broadcast messages.  

![](https://realpars.com/wp-content/uploads/2018/12/Modbus-Master-Query-and-Reply.gif#.XyuU72q5zLc.link)  


**MODBUS protocol can be used over 2 interfaces**  
1) RS485 - called as Modbus RTU
2) Ethernet - called as Modbus TCP/IP

## WHAT IS RS485?
* RS485 is a form of serial communication, it is much faster than RS232.
* You can put upto 32 devices on the same system.
* It is not directly compatiable, you must use correct time of interface or the signal won't go through.

## WHAT IS ETHERNET PROTOCOL?
* Ethernet is a communicatin device that was developed to network devices and multiple devices so that they can store and share information with other locations.
* Ethernet was started with co-axial cable and now has progressed in twisted pair copper wiring and fibre optic wiring.  
* Ethernet Physical layer consist of two components  
1) **Cabling**
2) **Devices**

## OPC UA PROTOCOL
* OPC UA(Open Platform Communiation Unified Architecture) is a data exchange standard for industrial communication.
* There are two mechanisms to exchange this data
1) A client-server model in which UA clients use the dedicated service of the UA server
2) A publisher-subscriber model in which the UA server makes a configurable subsets of information available to any number of recipients.  


![](https://www.novotek.com/images/solutionpages/Kepware_solutionpages/2015_OPC_client_server.png)

* In a client-server model you have one or more server that waits for several clients to make request.Once the server gets the request it answers it and go back into the wait state. It is the client that decides when and what data the server will fetch from the underlying system.
* In a pub-sub model the client subscribes to updates where the client decides how often the server should aquire the system.  
**OPC PROTOCOLS AND TYPES**
1) DA (Data Access)                    
2) AE(Alarm & Events)
3) HDA(Historical Data Analysis)
4) XML DA(XML Data Access)
5) DX (Data exchange)

# CLOUD PROTOCOLS
### MQTT(Message Queuing Telemetry Transport)
* MQTT protocol surrounds two subjects **client** and **broker**.
* This protocol uses a publish-subscribe communication pattern.
* MQTT broker is a server, where the client are the connected devices.
* When a device wants to send a data to a server(broker) it is called a publish. When the operation is reserved it is called subscribe.

 ![](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/358848b4b9222917e31db143ff0a6383/Main-entities-of-the-Message-Queuing-Telemetry-Transport-MQTT-protocol.png)

 * MQTT client as a publisher sends message to mqtt broker whose work is to distribute the message accordingly to all other MQTT clients subscribes to the topic on which publisher publishes the message.

**MQTT PROTOCOL APPLICATIONS AND USECASES**  
![](https://cdn.ttgtmedia.com/rms/onlineImages/MQTT_desktop.jpg)


# HTTP (HYPER TEXT TRANSFER PROTOCOL)

* Communication between client computer and web server is done by sending HTTP request and receiving HTTP responses.

### REQUEST
* Request has three parts
1) Request line
2) HTTP headers
3) Message body

**TYPES OF METHODS**
1) **GET**: Retrieve the resource from the server(Eg. Visiting a page)
2) **POST**: Create a resource on the server(Eg. submitting a form)
3) **PUT/PATCH**: Update resource on the server.
4) **DELETE**: Delete the resource on the server.

### RESPONSE   
* Response is made of three parts   
1) Status line
2) HTTP header
3) Message Body

**HTTP STATUS CODES**  


|Code | Description| Code| Description|
|-----|------------|-----|------------|
| 200 | OK | 400 |Bad Request |
|201|Created|401|Unauthorized|
|202|Accepted|403|Forbidden|
|301|Moved Permanently|401|Not found|
|303|See other|410|Gone|
|304|Not Modified|500|Internal Server Error|
|307|Temporary Redirect|503|Service Unavailable|


![](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/85f443d40e36e1f5bcb4e71ac14205b0/http-req-res.png)









