## DO'S AND DONT'S FOR POWER SUPPLY

| DO'S | DONT'S|
|------|-------|
|Make sure that ouput voltage of the power supply matches the input voltage of the board| Do not connect power supply without matching power rating|
|Make sure every circuit is connected properly| Never connect higher output(12V/3A) to lower input(5V/2A)|
||Do not try to force the connecter into the power socket, it damages the connecter|

## DO'S AND DONT'S OF HANDLING
| DO'S | DONT'S|
|------|-------|
|Treat every device like it is energized, even if it does not look like plugges in or operational| Do not handle board when it is powered ON|
| While working keep the board on flat stable surface| Never touch electrical equipment when any part of the body is wet|
|Unplug the device before performing any operation on them.|Do not touch any sort of metal to the development board.|
|When handling electrical equipment, make sure your hands are dry.||
|Keep all electrical circuit contact points enclosed.||
|If the board becomes too hot try to cool it with a external usb fan||

## DO'S AND DONT'S OF GPIO
| DO'S | DONT'S|
|------|-------|
|Find wheather the board runs on 3.3v of 5v logic|Never connect greater than 5v to a 3.3v pin|
|Always connect the LED (or sensors) using appropriate resistors .|Avoid making connections when the board is running.|
|To Use 5V peripherals with 3.3V we require a logic level converter.|Don't plug anything with a high (or negative) voltage.|
||Do not connect a motor directly , use a transistor to drive it|

## GUIDELINES FOR USING UART  
>**1) Connect Rx pin of device1 to Tx pin of device2 ,similarly Tx pin of device1 to Rx pin of device2.**  
>**2) If the device1 works on 5v and device2 works at 3.3v then use the level shifting mechanism(voltage divider )**  
>**3) Genrally UART is used to communicate with board through USB to TTL connection . USB to TTL connection does not require a protection circuit .**  
>**4) Whereas Senor interfacing using UART might require a protection circuit.** 

## GUIDELINES FOR USING I2C 
>**1) while using I2c interfaces with sensors SDA and SDL lines must be protected.**  
>**2) Protection of these lines is done by using pullup registers on both lines.**  
>**3) If you use the inbuilt pullup registers in the board you wont need an external circuit.**  
>**4) if you are using bread-board to connect your sensor , use the pullup resistor .** 
>**5) Generally , 2.2kohm <= 4K ohm resistors are used.**  

## GUIDELINES FOR USING SPI
>**1) Generally ,Spi in development boards is in Push-pull mode.**  
>**2) Push-pull mode does not require any protection circuit.**  
>**3) on Spi interface if you are using more than one slaves it is possible that the device2 can "hear" and "respond" to the master's communication with device1- which is an disturbance.**  
>**4) To overcome this problem , we use a protection circuit with pullup resistors on each the Slave Select line(CS).** 
>**5) Resistors value can be between 1kOhm ~10kOhm . Generally 4.7kOhm resistor is used.** 























